using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class RotateCube : MonoBehaviour
{
    public  InputAction activateRotation ;
    private bool rot=false;
    public float speed = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();
        activateRotation.performed += ctx => rot=!rot;
        Debug.Log ("Touche R cliqué");


    }

    // Update is called once per frame
    void Update()
    {
     // Rotation de 3 degrés autour de l'axe vertical (Y)
     if(rot)
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }
}
