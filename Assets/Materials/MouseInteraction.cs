using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Rigidbody rb;
    private Renderer rend;
    private Color initialColor;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        initialColor = rend.material.color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        rb.AddForce(Camera.main.transform.forward * 1000f, ForceMode.Force);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        rend.material.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rend.material.color = initialColor;
    }
}
